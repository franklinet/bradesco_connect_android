package com.cognizant.bradesco

import android.content.Context
import android.arch.persistence.room.*
import java.util.Date
import java.util.Calendar

enum class Duration(val period: Int) {
    DAY(0), MONTH(1), YEAR(2)
}

@Entity
data class App(
    @ColumnInfo(name = "name")
    var name: String = "",

    @ColumnInfo(name = "account")
    var account: String = "",

    @ColumnInfo(name = "appPackageName")
    var appPackageName: String = "",

    @ColumnInfo(name = "allowed_at")
    var allowedAt: Date = Date(),

    @ColumnInfo(name = "duration")
    var duration: Duration = Duration.DAY,

    @ColumnInfo(name = "permitted")
    var permitted: Boolean = false
) {

    @PrimaryKey
    var uid: Int = -1

    fun isAllowed(): Boolean {
        if (!permitted) return false

        val cal = Calendar.getInstance()
        cal.setTime(allowedAt)

        when (duration) {
            Duration.DAY -> cal.add(Calendar.DAY_OF_YEAR, 1)
            Duration.MONTH -> cal.add(Calendar.MONTH, 1)
            Duration.YEAR -> cal.add(Calendar.YEAR, 1)
        }

        return Date().before(cal.getTime())
    }
}

class Converters {
    companion object {
        @JvmStatic @TypeConverter fun fromInt(value: Int): Duration {
            return when (value) {
                0 -> Duration.DAY
                1 -> Duration.MONTH
                2 -> Duration.YEAR
                else -> throw IllegalStateException()
            }
        }

        @JvmStatic @TypeConverter fun durationToInt(duration: Duration): Int {
            return duration.period
        }

        @JvmStatic @TypeConverter fun fromTimestamp(value: Long?): Date? {
            return Date(value ?: return null)
        }

        @JvmStatic @TypeConverter fun dateToTimestamp(date: Date?): Long? {
            return date?.time
        }
    }
}

@Database(entities = arrayOf(App::class), version = 3)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun appDao() : AppDao
}

@Dao
interface AppDao {
    @Query("SELECT * FROM app WHERE account = :account")
    fun getAll(account: String): List<App>

    @Query("SELECT * FROM app WHERE appPackageName = :appPackageName AND account = :account")
    fun getApp(appPackageName: String, account: String): App?

    @Delete
    fun delete(app: App)

    @Update
    fun update(app: App)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(app: App)
}

fun database(context: Context): AppDatabase {
    return Room.databaseBuilder(context, AppDatabase::class.java, "app-database")
    .fallbackToDestructiveMigration().build()
}
