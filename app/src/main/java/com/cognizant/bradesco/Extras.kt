package com.cognizant.bradesco

object Extras {
    const val ACCESS_TOKEN = "access_token"
    const val USER_NAME = "user_name"
    const val USER_ACCOUNT_NAME = "user_account_name"
    const val USER_PASSWORD = "user_password"
    const val APP_NAME = "app_name"
    const val APP_PACKAGE_NAME = "app_packageName"
    const val BRANCH_NUMBER = "branch_number"
    const val ACCOUNT_NUMBER = "account_number"
    const val OTP = "otp"
}
