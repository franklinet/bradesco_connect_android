package com.cognizant.bradesco

import android.app.Application
import android.content.Context
import android.content.Intent
import com.ca.mas.foundation.MAS
import com.cognizant.bradesco.modules.AuthenticationActivity

class ConnectApplication : Application() {

    override fun onCreate() {
        MAS.start(this)
        MAS.debug()
    }
}
