package com.cognizant.bradesco.modules

import android.app.AlertDialog
import android.content.DialogInterface
import kotlin.concurrent.thread
import android.os.Handler
import android.os.Looper
import android.graphics.Typeface
import android.text.SpannableStringBuilder
import android.text.Spannable
import android.text.style.StyleSpan
import android.os.Bundle
import java.util.Date
import android.view.ViewGroup
import android.view.View
import android.support.v7.widget.RecyclerView
import android.content.Intent
import android.util.Log
import android.support.constraint.ConstraintLayout
import android.support.constraint.ConstraintSet
import android.view.LayoutInflater;
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import android.view.MenuItem
import android.widget.RadioGroup
import android.widget.RadioButton
import android.support.v7.app.AppCompatActivity
import com.cognizant.bradesco.R
import com.cognizant.bradesco.App
import com.cognizant.bradesco.Duration
import com.cognizant.bradesco.Converters
import com.cognizant.bradesco.database
import com.ca.mas.foundation.MASUser
import com.ca.mas.foundation.MASCallback
import kotlinx.android.synthetic.main.activity_info.*
import android.widget.PopupMenu
import kotlinx.android.synthetic.main.view_app.view.*

class InfoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        if (MASUser.getCurrentUser() == null) {
            Toast.makeText(this, "Por favor, refaça o login.", Toast.LENGTH_SHORT).show()
            finish()
        }

        name_txt.text = getSharedPreferences("user", 0).getString("user_account_name", "")
        branch_txt.text = "Ag. " + getSharedPreferences("user", 0).getString("branch", "")
        account_txt.text = "C/C " +  getSharedPreferences("user", 0).getString("account", "")

        logout_btn.setOnClickListener {
            MASUser.getCurrentUser()?.logout(object : MASCallback<Void>() {
                override fun onSuccess(result: Void?) {
                    runOnUiThread {
                        finish()
                    }
                }
                override fun onError(e: Throwable) {
                    runOnUiThread {
                        Toast.makeText(this@InfoActivity, e.localizedMessage, Toast.LENGTH_SHORT).show()
                    }
                }
            }) ?: finish()
        }
    }

    override fun onResume() {
        super.onResume()

        if (MASUser.getCurrentUser() == null) {
            Toast.makeText(this, "Por favor, refaça o login.", Toast.LENGTH_SHORT).show()
            finish()
        }

        val account = getSharedPreferences("user", 0).getString("branch", "") +
            getSharedPreferences("user", 0).getString("account", "")

        thread {
            val list = database(this).appDao().getAll(account).toMutableList()
            runOnUiThread {
                app_recycler.adapter = AppListAdapter(list)
                app_recycler.layoutManager = LinearLayoutManager(this)
            }
        }
    }
}

class AppListAdapter(val mList: MutableList<App>) : RecyclerView.Adapter<AppListAdapter.ViewHolder>() {

    override fun getItemCount() = mList.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppListAdapter.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.view_app, parent, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: AppListAdapter.ViewHolder, position: Int) {
        holder.name.setText("Applicativo ${mList[position].name}")

        val timeText = when(mList[position].duration) {
            Duration.DAY -> "1 dia"
            Duration.MONTH -> "1 mês"
            Duration.YEAR -> "1 ano"
        }.run {
            SpannableStringBuilder("Tempo de Permissão: ").also {
                val start = it.length
                it.append(this)
                it.setSpan(StyleSpan(Typeface.BOLD), start, it.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
        }

        holder.time.setText(timeText)
        (holder.timeRg.getChildAt(mList[position].duration.period) as RadioButton).setChecked(true)

        holder.optionBtn.setOnClickListener {
            PopupMenu(holder.itemView.context, holder.optionBtn).apply {
                setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(menuItem: MenuItem): Boolean {
                        when (menuItem.itemId) {
                            R.id.action_remove -> {
                                AlertDialog.Builder(holder.itemView.context)
                                    .setTitle("Remover Permissão")
                                    .setMessage("Tem certeza que deseja remover a permissão do aplicativo JuntAí?")
                                    .setPositiveButton("Remover", object : DialogInterface.OnClickListener {
                                        override fun onClick(dialog: DialogInterface, which: Int) {
                                            thread {
                                                val appDao = database(holder.itemView.context).appDao()
                                                appDao.delete(mList[position])
                                                runOnUiThread {
                                                    mList.remove(mList[position])
                                                    notifyDataSetChanged()
                                                }
                                            }
                                        }
                                    }).setNegativeButton("Cancelar", null).show()
                            }
                            R.id.action_change -> {
                                val constraitSet = ConstraintSet()
                                constraitSet.clone(holder.itemView.constraint)
                                constraitSet.constrainHeight(
                                        R.id.expandable,
                                        ConstraintLayout.LayoutParams.WRAP_CONTENT
                                )
                                constraitSet.applyTo(holder.itemView.constraint)
                            }
                        }
                        return true
                    }
                })
                inflate(R.menu.app_menu)
                show()
            }
        }

        holder.saveBtn.setOnClickListener {
            val id = holder.timeRg.checkedRadioButtonId

            val time = when (id) {
                R.id.rb1day -> 0
                R.id.rb1month -> 1
                R.id.rb1year -> 2
                else -> throw IllegalStateException()
            }

            thread {
                val appDao = database(holder.itemView.context).appDao()

                mList[position].let {
                    it.permitted = true
                    it.allowedAt = Date()
                    it.duration = Converters.fromInt(time)
                    appDao.update(it)
                    Log.d("Bradesco", "$it updated")
                }
                runOnUiThread {
                    val constraitSet = ConstraintSet()
                    constraitSet.clone(holder.itemView.constraint)
                    constraitSet.constrainHeight(
                            R.id.expandable,
                            1
                    )
                    constraitSet.applyTo(holder.itemView.constraint)
                    notifyDataSetChanged()
                }
            }
        }
    }

    private fun runOnUiThread(block: () -> Unit) {
        Handler(Looper.getMainLooper()).post(block)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.label_app_name
        val time = itemView.label_app_time
        val optionBtn = itemView.app_options_btn
        val expandable = itemView.expandable
        val saveBtn = itemView.save_change_btn
        val timeRg = itemView.time_rg
    }
}
