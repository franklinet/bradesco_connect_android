package com.cognizant.bradesco.modules

import kotlin.concurrent.thread
import android.util.Log
import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.cognizant.bradesco.database
import com.ca.mas.foundation.MASUser
import com.cognizant.bradesco.Extras

class StatusActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (MASUser.getCurrentUser() == null) {
            setResult(Activity.RESULT_CANCELED, null)
            finish()
        }

        val loginBranch = getSharedPreferences("user", 0).getString("branch", "")
        val loginAccount = getSharedPreferences("user", 0).getString("account", "")

        val statusBranch = intent.getStringExtra(Extras.BRANCH_NUMBER)
        val statusAccount = intent.getStringExtra(Extras.ACCOUNT_NUMBER)
        val appPackageName = intent.getStringExtra(Extras.APP_PACKAGE_NAME)

        Log.d("Bradesco", "Status request by $appPackageName for $statusBranch $statusAccount")

        if (loginBranch + loginAccount != statusBranch + statusAccount) {
            Log.d("Bradesco", "Status request: mismatched account")
            setResult(Activity.RESULT_CANCELED, null)
            finish()
        }


        thread {
            val appDao = database(this).appDao()
            val app = appDao.getApp(appPackageName, statusBranch + statusAccount)
            runOnUiThread {
                if (app?.isAllowed() ?: false) {
                    Log.d("Bradesco", "Status request: app allowed")
                    setResult(Activity.RESULT_OK, null)
                    finish()
                } else {
                    Log.d("Bradesco", "Status request: app not allowed")
                    setResult(Activity.RESULT_CANCELED, null)
                    finish()
                }
            }
        }
    }
}
