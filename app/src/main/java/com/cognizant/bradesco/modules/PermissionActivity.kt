package com.cognizant.bradesco.modules

import kotlin.concurrent.thread
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.text.SpannableStringBuilder
import android.text.Spannable
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.app.Activity
import com.ca.mas.foundation.MASUser
import com.ca.mas.foundation.MASCallback
import android.content.Intent
import android.widget.Toast
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.RadioGroup
import java.util.Date
import com.cognizant.bradesco.Extras
import com.cognizant.bradesco.database
import com.cognizant.bradesco.App
import com.cognizant.bradesco.AppDao
import com.cognizant.bradesco.Converters
import com.cognizant.bradesco.R
import kotlinx.android.synthetic.main.activity_permission.*

class PermissionActivity : AppCompatActivity() {

    var app: App? = null
    var appDao: AppDao? = null
    var branch = ""
    var account = ""
    var appName = ""
    var appPackageName = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (MASUser.getCurrentUser() == null) {
            Toast.makeText(this, "Por favor, refaça o login.", Toast.LENGTH_SHORT).show()
            finish()
        }

        branch = getSharedPreferences("user", 0).getString("branch", "")
        account = getSharedPreferences("user", 0).getString("account", "")

        appName = intent.getStringExtra(Extras.APP_NAME)
        appPackageName = intent.getStringExtra(Extras.APP_PACKAGE_NAME)

        Log.d("Bradesco", "Permission (${branch + account}) request $appName: $appPackageName")

        thread {
            appDao = database(this).appDao()
            app = appDao?.getApp(appPackageName, branch + account)
            Log.d("Bradesco", "Query result: $app")

            runOnUiThread {
                if (app?.isAllowed() ?: false) {
                    Log.d("Bradesco", "$appName allowed")
                    setResult(Activity.RESULT_OK, null)
                    finish()
                } else {
                    Log.d("Bradesco", "$appName not allowed")
                    setContentView(R.layout.activity_permission)
                    permission_txv_app_name.text = appName
                    setupEvents()
                }
            }
        }
    }

    private fun setupEvents() {

        permission_btn_deny.setOnClickListener {
            Log.d("Bradesco", "$appName denied")
            finish()
        }

        permission_btn_grant.setOnClickListener {
            val id = time_rg.checkedRadioButtonId

            val time = when (id) {
                R.id.rb1day -> 0
                R.id.rb1month -> 1
                R.id.rb1year -> 2
                else -> throw IllegalStateException()
            }

            Log.d("Bradesco", "$appName granted $time")

            permission_btn_deny.setEnabled(false)
            permission_btn_grant.setEnabled(false)

            thread {
                if (app == null) {
                    val newApp = App(
                            appName,
                            branch + account,
                            appPackageName,
                            Date(),
                            Converters.fromInt(time),
                            true
                    )
                    appDao!!.insert(newApp)
                    Log.d("Bradesco", "$appName for ${newApp.account} inserted")
                } else {
                    app?.let {
                        it.permitted = true
                        it.allowedAt = Date()
                        it.duration = Converters.fromInt(time)
                        appDao!!.update(it)
                        Log.d("Bradesco", "$appName updated")
                    }
                }

                runOnUiThread {
                    val resultIntent = Intent()
                    setResult(Activity.RESULT_OK, resultIntent)
                    finish()
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (!isFinishing) finish()
    }
}
