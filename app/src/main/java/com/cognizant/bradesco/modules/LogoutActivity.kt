package com.cognizant.bradesco.modules

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import android.support.v7.app.AppCompatActivity
import com.ca.mas.foundation.MASUser
import com.ca.mas.foundation.MASCallback

public class LogoutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (MASUser.getCurrentUser() == null) {
            Toast.makeText(this, "Por favor, refaça o login.", Toast.LENGTH_SHORT).show()
            finish()
        }

        MASUser.getCurrentUser()?.logout(object : MASCallback<Void>() {
            override fun onSuccess(result: Void?) { logoutComplete() }
            override fun onError(e: Throwable) { logoutComplete() }
        }) ?: logoutComplete()
    }

    fun logoutComplete() {
        runOnUiThread {
            setResult(Activity.RESULT_OK, null)
            finish()
        }
    }
}
