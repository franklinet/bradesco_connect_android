package com.cognizant.bradesco.modules

import android.Manifest
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import android.support.v4.app.ActivityCompat
import java.lang.Math.abs
import android.widget.Toast
import com.cognizant.bradesco.R
import android.content.Intent
import java.util.Date
import java.text.SimpleDateFormat
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_payment_complete.*

class TransferComplete : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_complete)

        origin_name.text = getSharedPreferences("user", 0).getString("user_account_name", "")
        origin_branch.text = "Ag " + getSharedPreferences("user", 0).getString("branch", "")
        origin_number.text = "C/C " + getSharedPreferences("user", 0).getString("account", "")

        origin_date.text = SimpleDateFormat("dd-MM-yyyy HH:mm").format(Date())

        val branch = intent?.extras?.getString("branch") ?: ""
        val number = intent?.extras?.getString("number") ?: ""
        val amount = intent?.extras?.getFloat("amount") ?: 0f
        val description = intent?.extras?.getString("description") ?: ""

        dest_branch.setText("Ag $branch")
        dest_number.setText("C/C $number")
        amount_txt.setText(amount.formatAsCurrency())

        back_btn.setOnClickListener {
            finish()
        }
        share_btn.setOnClickListener {
            withPermission {
                card_view.setDrawingCacheEnabled(true)
                val bitmap = android.graphics.Bitmap.createBitmap(card_view.getDrawingCache())
                card_view.setDrawingCacheEnabled(false)

                bitmap.compress(android.graphics.Bitmap.CompressFormat.JPEG, 100,
                    java.io.ByteArrayOutputStream())

                val path =
                    android.provider.MediaStore.Images.Media.insertImage(
                            contentResolver, bitmap, origin_date.text.toString(), null)
                val uri = android.net.Uri.parse(path)
                val intent = Intent(Intent.ACTION_SEND)
                intent.setType("image/jpeg")
                intent.putExtra(Intent.EXTRA_STREAM, uri)
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(intent, "Enviar comprovante para..."))
            }
        }
    }

    private fun Float.formatAsCurrency(): String {
        return buildString {
            if (this@formatAsCurrency < 0) append("- ")
            append(String.format("R$%.2f", abs(this@formatAsCurrency)))
        }
    }


    private val WRITE_EXTERNAL_STORAGE_REQUEST = 1
    private var permissionBlock: () -> Unit = {}
    private fun withPermission(block: () -> Unit) {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        WRITE_EXTERNAL_STORAGE_REQUEST)
                permissionBlock = block
        } else {
            block()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            WRITE_EXTERNAL_STORAGE_REQUEST -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    permissionBlock.invoke()
                } else {
                    Toast.makeText(this, "Permissão necessário para compartilhar comprovante", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }
}
