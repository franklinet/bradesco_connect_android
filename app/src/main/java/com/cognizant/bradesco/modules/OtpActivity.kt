package com.cognizant.bradesco.modules

import android.app.Activity
import android.os.Bundle
import android.content.Intent
import android.widget.Toast
import android.support.v7.app.AppCompatActivity
import android.net.Uri
import android.view.View
import android.animation.Animator
import android.animation.ObjectAnimator
import android.view.animation.LinearInterpolator
import com.ca.mas.foundation.MAS
import com.ca.mas.foundation.MASCallback
import com.ca.mas.foundation.MASRequest
import com.ca.mas.foundation.MASResponse
import com.ca.mas.foundation.MASResponseBody
import com.cognizant.bradesco.Extras
import com.cognizant.bradesco.R
import org.json.JSONObject
import kotlinx.android.synthetic.main.activity_otp.*

class OtpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)

        if (!(intent?.extras?.getBoolean("fromApplication", false) ?: false)) {
            copy_otp_btn.visibility = View.GONE
        }

        copy_otp_btn.setOnClickListener {
            val resultIntent = Intent()
            resultIntent.putExtra(Extras.OTP, otp_txt.text.toString())
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
        }

        requestOtp()
    }

    override fun onPause() {
        super.onPause()
        if (!isFinishing) finish()
    }

    private fun updateOtp(otp: String) {
        otp_txt.text = otp

        ObjectAnimator.ofInt(otp_timer_bar, "progress", 100, 0).let {
            it.setDuration(20000)
            it.setInterpolator(LinearInterpolator())
            it.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) = Unit
                override fun onAnimationCancel(animator: Animator) = Unit
                override fun onAnimationRepeat(animator: Animator) = Unit
                override fun onAnimationEnd(animator: Animator) {
                    requestOtp()
                }
            })
            it.start()
        }
    }

    private fun requestOtp() {
        val path = "/auth/generateOTP"
        val uri = Uri.Builder().encodedPath(path).build()
        val request = MASRequest.MASRequestBuilder(uri)
            .get()
            .responseBody(MASResponseBody.jsonBody())
            .build()

        MAS.invoke(request, object : MASCallback<MASResponse<JSONObject>>() {
            override fun onSuccess(result: MASResponse<JSONObject>?) {
                runOnUiThread {
                    try {
                        updateOtp(result?.body?.content?.getString("codeOTP") ?: "")
                    } catch (e: Throwable) {
                        Toast.makeText(this@OtpActivity, e.localizedMessage, Toast.LENGTH_SHORT).show()
                    }
                }
            }
            override fun onError(e: Throwable) {
                runOnUiThread {
                    Toast.makeText(this@OtpActivity, e.localizedMessage, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

}
