package com.cognizant.bradesco.modules

import java.lang.Math.abs
import android.app.Activity
import android.content.Intent
import android.view.View
import android.net.Uri
import java.util.concurrent.TimeUnit
import android.widget.Toast
import android.animation.Animator
import android.animation.ObjectAnimator
import android.view.animation.LinearInterpolator
import com.ca.mas.foundation.MAS
import com.ca.mas.foundation.MASUser
import com.ca.mas.foundation.MASCallback
import com.ca.mas.foundation.MASRequest
import com.ca.mas.foundation.MASResponse
import com.ca.mas.foundation.MASResponseBody
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.RadioGroup
import com.cognizant.bradesco.Extras
import com.cognizant.bradesco.R
import org.json.JSONObject
import kotlinx.android.synthetic.main.activity_payment.*

import okhttp3.OkHttpClient
import com.google.gson.annotations.SerializedName
import okhttp3.logging.HttpLoggingInterceptor
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.X509TrustManager
import okhttp3.MediaType
import com.google.gson.Gson
import okhttp3.Response
import okhttp3.Request
import okhttp3.RequestBody
import kotlin.concurrent.thread

private val trustManager = object : X509TrustManager {
    override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) = Unit
    override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) = Unit
    override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()
}

private val sslSocketFactory: SSLSocketFactory = SSLContext.getInstance("SSL").run {
    init(null, arrayOf(trustManager), SecureRandom())
    socketFactory
}

val untrustedOkHttpClient: OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .sslSocketFactory(sslSocketFactory, trustManager)
        .hostnameVerifier({ _, _ -> true })
        .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
        .build()

public class PaymentActivity : AppCompatActivity() {

    private var branch = ""
    private var number = ""
    private var amount = 0f
    private var description = ""
    private var attempts = 2
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        if (MASUser.getCurrentUser() == null) {
            Toast.makeText(this, "Por favor, refaça o login.", Toast.LENGTH_SHORT).show()
            setResult(Activity.RESULT_CANCELED, null)
            finish()
        }

        origin_name.text = getSharedPreferences("user", 0).getString("user_account_name", "")
        origin_branch.text = "Ag " + getSharedPreferences("user", 0).getString("branch", "")
        origin_number.text = "C/C " + getSharedPreferences("user", 0).getString("account", "")

        branch = intent?.extras?.getString("branch") ?: ""
        number = intent?.extras?.getString("number") ?: ""
        amount = intent?.extras?.getFloat("amount") ?: 0f
        description = intent?.extras?.getString("description") ?: ""

        dest_branch.setText("Ag $branch")
        dest_number.setText("C/C $number")
        amount_txt.setText(amount.formatAsCurrency())
        requestOtp()

        confirm_btn.setOnClickListener {
            otpAnimator?.cancel()
            confirm_btn.setEnabled(false)
            otp_timer_bar.visibility = View.GONE
            confirm_btn.visibility = View.GONE
            otp_txt.visibility = View.GONE
            progress.visibility = View.VISIBLE
            transfer()
        }
    }

    private fun transfer() {
        attempts--
        val transferRequest = TransferRequest(branch, number, description, amount)
        transferMoney(transferRequest, MASUser.getCurrentUser().accessToken, otp_txt.text.toString(), {
            runOnUiThread {
                setResult(Activity.RESULT_OK, null)
                confirm_btn.setEnabled(true)
                otp_timer_bar.visibility = View.VISIBLE
                confirm_btn.visibility = View.VISIBLE
                otp_txt.visibility = View.VISIBLE
                progress.visibility = View.GONE
                /*val completeIntent = Intent(this@PaymentActivity, TransferComplete::class.java)*/
                /*completeIntent.apply {*/
                    /*putExtra("branch", branch)*/
                    /*putExtra("number", number)*/
                    /*putExtra("amount", amount)*/
                    /*putExtra("description", description)*/
                /*}*/
                /*startActivity(completeIntent)*/
                otpAnimator?.cancel()
                finish()
            }
        }, { e ->
            runOnUiThread {
                /*setResult(Activity.RESULT_OK, null)*/
                /*confirm_btn.setEnabled(true)*/
                /*val completeIntent = Intent(this@PaymentActivity, TransferComplete::class.java)*/
                /*completeIntent.apply {*/
                    /*putExtra("branch", branch)*/
                    /*putExtra("number", number)*/
                    /*putExtra("amount", amount)*/
                    /*putExtra("description", description)*/
                /*}*/
                /*startActivity(completeIntent)*/
                /*finish()*/
                requestOtp()
                confirm_btn.setEnabled(true)
                confirm_btn.visibility = View.VISIBLE
                otp_timer_bar.visibility = View.VISIBLE
                otp_txt.visibility = View.VISIBLE
                progress.visibility = View.GONE
                Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
                setResult(Activity.RESULT_CANCELED, null)
            }
        })
    }

    private fun endpoint(path: String): String {
        require(path.isNotBlank()) { "path must not be empty or blank" }
        return BASE_URL + path
    }

    fun transferMoney(
            request: TransferRequest,
            token: String,
            otp: String,
            onSuccess: () -> Unit,
            onFailure: (Exception) -> Unit
    ) {
        val json = Gson().toJson(request)

        val requestBody = RequestBody.create(MediaType.parse("application/json"), json)
        val call = Request.Builder()
                .url(endpoint("transactions/moneyTransfer"))
                .post(requestBody)
                .header("Authorization", "Bearer $token")
                .header("X-OTP", otp)
                .build().let {
                    untrustedOkHttpClient.newCall(it)
                }

        thread {
            try {
                val response = call.execute()
                when {
                    response.code() == 204 -> onSuccess()
                    response.code() == 200 -> {
                        val status = response.fromJson<StatusResponse>()
                        onFailure(ApiException(status.status))
                    }
                    else -> {
                        if (attempts <= 0) {
                            onFailure(ApiException("Unknown error response: ${response.code()}"))
                        } else {
                            requestOtp {
                                transferMoney(request, token, otp_txt.text.toString(), onSuccess, onFailure)
                            }
                        }
                    }
                }
            } catch (exception: Exception) {
                onFailure(exception)
            }
        }
    }

    private var otpAnimator: ObjectAnimator? = null
    private fun updateOtp(otp: String) {
        otp_txt.text = otp

        otpAnimator = ObjectAnimator.ofInt(otp_timer_bar, "progress", 0, 100)
        otpAnimator?.let {
            it.setDuration(20000)
            it.setInterpolator(LinearInterpolator())
            it.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) = Unit
                override fun onAnimationCancel(animator: Animator) = Unit
                override fun onAnimationRepeat(animator: Animator) = Unit
                override fun onAnimationEnd(animator: Animator) {
                    requestOtp()
                }
            })
            it.start()
        }
    }

    private inline fun <reified T> Response.fromJson(): T {
        val json = body()?.string() ?:
            throw ApiException("Error: empty server response")
        return Gson().fromJson<T>(json, T::class.java)
    }

    private fun requestOtp(callback: () -> Unit = {}) {
        val path = "/auth/generateOTP"
        val uri = Uri.Builder().encodedPath(path).build()
        val request = MASRequest.MASRequestBuilder(uri)
            .get()
            .responseBody(MASResponseBody.jsonBody())
            .build()

        MAS.invoke(request, object : MASCallback<MASResponse<JSONObject>>() {
            override fun onSuccess(result: MASResponse<JSONObject>?) {
                runOnUiThread {
                    try {
                        updateOtp(result?.body?.content?.getString("codeOTP") ?: "")
                        callback()
                    } catch (e: Throwable) {
                        Toast.makeText(this@PaymentActivity, e.localizedMessage, Toast.LENGTH_SHORT).show()
                    }
                }
            }
            override fun onError(e: Throwable) {
                runOnUiThread {
                    Toast.makeText(this@PaymentActivity, e.localizedMessage, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun Float.formatAsCurrency(): String {
        return buildString {
            if (this@formatAsCurrency < 0) append("- ")
            append(String.format("R$%.2f", abs(this@formatAsCurrency)))
        }
    }
}
class ApiException(message: String) : Exception(message)
data class TransferRequest(
        @SerializedName("recipientCodeBranch") val recipientBranch: String,
        @SerializedName("recipientAccountNumber") val recipientAccount: String,
        @SerializedName("description") val description: String,
        @SerializedName("amount") val amount: Float
)
data class StatusResponse(
        @SerializedName("status") var status: String = ""
)
private const val BASE_URL = "https://lab6.prebanco.com.br:8443/v2/"
