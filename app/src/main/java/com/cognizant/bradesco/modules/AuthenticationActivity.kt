package com.cognizant.bradesco.modules

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import android.support.v7.app.AppCompatActivity
import com.ca.mas.foundation.auth.MASAuthenticationProviders
import com.ca.mas.foundation.MASAuthenticationListener
import com.ca.mas.foundation.MASOtpAuthenticationHandler
import com.ca.mas.foundation.MASCallback
import com.ca.mas.foundation.MASUser
import com.ca.mas.foundation.MAS
import com.cognizant.bradesco.Extras
import com.cognizant.bradesco.R
import kotlinx.android.synthetic.main.activity_autentication.*

class AuthenticationActivity : AppCompatActivity() {

    enum class Action {
        LOGIN,
        CONSENT,
        OTP
    }

    private var action: Action = Action.LOGIN
    private var loginInProgress = false
    private var startingActivity = false

    private fun setLoading(loading: Boolean) {
        login_container.visibility = if (loading) View.GONE else View.VISIBLE
        loading_container.visibility = if (loading) View.VISIBLE else View.GONE
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_autentication)

        setLoading(true)

        action = when(intent.action) {
            "com.cognizant.bradesco.modules.AUTHENTICATION" -> {
                Action.CONSENT
            }
            "com.cognizant.bradesco.modules.OTP" -> {
                Action.OTP
            }
            else -> Action.LOGIN
        }

        MAS.setAuthenticationListener(object : MASAuthenticationListener {
            override fun onAuthenticateRequest(
                    context: Context?,
                    requestId: Long,
                    providers: MASAuthenticationProviders
            ) {
                if (context == null) return
                runOnUiThread {
                    setLoading(false)
                }
            }
            override fun onOtpAuthenticateRequest(
                    context: Context,
                    handler: MASOtpAuthenticationHandler
            ) = Unit
        })

        MASUser.login(object : MASCallback<MASUser>() {
            override fun onSuccess(result: MASUser) {
                runOnUiThread {
                    startActionActivity()
                    startingActivity = true
                }
            }
            override fun onError(e: Throwable) {
                runOnUiThread {
                    Toast.makeText(this@AuthenticationActivity, e.localizedMessage, Toast.LENGTH_SHORT).show()
                    setLoading(false)
                }
            }
        })

        login_button_enter.setOnClickListener {
            val token = currentFocus?.windowToken

            if (token != null) {
                val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS)
            }

            if (loginInProgress) return@setOnClickListener
            loginInProgress = true
            doLogin()
        }
    }

    private val nameMap = mapOf(
        "99" to "Julia Ulia",
        "100" to "Herbert Zum",
        "101" to "Fabio Guim",
        "102" to "Henrique Trim",
        "103" to "Elder Min"
    )

    private fun doLogin() {
        setLoading(true)
        val user = login_edt_branch.text.toString() + login_edt_account.text.toString()
        val password = login_edt_password.text.toString().toCharArray()
        try {
            MASUser.login(user, password, object : MASCallback<MASUser>() {
                override fun onSuccess(result: MASUser?) {
                    getSharedPreferences("user", 0).edit().let {
                        it.putString("login_time", System.currentTimeMillis().toString())
                        it.putString("branch", login_edt_branch.text.toString())
                        it.putString("account", login_edt_account.text.toString())
                        val name = nameMap[login_edt_account.text.toString()] ?: "Darth Vader"
                        it.putString("user_account_name", name)
                        it.commit()
                    }

                    loginInProgress = false
                    startActionActivity()
                    startingActivity = true
                    Log.i("MAS", "Result - " + result?.asJSONObject?.toString())
                }

                override fun onError(e: Throwable?) {
                    runOnUiThread {
                        setLoading(false)
                        loginInProgress = false
                        Log.e("MAS", e?.message)
                    }
                }
            })
        } catch (e : Exception) {
            setLoading(false)
            loginInProgress = false
        }
    }

    private fun startActionActivity() {
        if (startingActivity) return
        when (action) {
            Action.CONSENT -> {
                val permissionIntent =
                    Intent(this, PermissionActivity::class.java)
                permissionIntent.putExtra(
                        Extras.APP_NAME,
                        intent.getStringExtra(Extras.APP_NAME)
                )
                permissionIntent.putExtra(
                        Extras.APP_PACKAGE_NAME,
                        intent.getStringExtra(Extras.APP_PACKAGE_NAME)
                )
                startActivityForResult(permissionIntent, 2)
            }
            Action.LOGIN -> {
                val infoIntent =
                    Intent(this, InfoActivity::class.java)
                startActivity(infoIntent)
                finish()
            }
            Action.OTP -> {
                val otpIntent =
                    Intent(this, OtpActivity::class.java)
                otpIntent.putExtra("fromApplication", true)
                startActivityForResult(otpIntent, 1)
            }
        }
    }

    private fun loadCredentials() {
        val user = login_edt_branch.text.toString() + login_edt_account.text.toString()
        val password = login_edt_password.text.toString()

        val permissionIntent = Intent(this, PermissionActivity::class.java)
        permissionIntent.putExtra(Extras.USER_NAME, user)
        permissionIntent.putExtra(Extras.USER_PASSWORD, password)
        permissionIntent.putExtra(Extras.APP_NAME, intent.getStringExtra(Extras.APP_NAME))

        startActivityForResult(permissionIntent, 2)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            val resultIntent = Intent()
            when (action) {
                Action.CONSENT -> {
                    resultIntent.putExtra(Extras.ACCESS_TOKEN, MASUser.getCurrentUser().accessToken)
                    resultIntent.putExtra(Extras.BRANCH_NUMBER, getSharedPreferences("user", 0).getString("branch", ""))
                    resultIntent.putExtra(Extras.ACCOUNT_NUMBER, getSharedPreferences("user", 0).getString("account", ""))
                    resultIntent.putExtra(Extras.USER_ACCOUNT_NAME, getSharedPreferences("user", 0).getString("user_account_name", ""))
                }
                Action.OTP -> {
                    resultIntent.putExtra(Extras.OTP, data?.extras?.getString(Extras.OTP, "") ?: "")
                }
                Action.LOGIN -> { }
            }
            setResult(Activity.RESULT_OK, resultIntent)
        }
        finish()
    }

}
