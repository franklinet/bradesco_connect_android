package com.cognizant.bradesco

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.cognizant.bradesco.modules.AuthenticationActivity
import com.cognizant.bradesco.modules.InfoActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        loadInfoActivity()
    }

    private fun loadInfoActivity() {

        Handler().postDelayed({
            val intent = Intent(this, AuthenticationActivity::class.java)
            startActivity(intent)
            finish()
        }, 2*1000)

    }
}
